package com.bbva.uuaa.batch;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.core.io.Resource;

public class BorrarArchivo implements Tasklet{
	
	Resource resource;
	
	public Resource getResource() {
		return resource;
	}
	
	public void setResource(Resource resource) {
		this.resource= resource;
	}
	
	@Override
	
	public RepeatStatus execute (StepContribution arg0, ChunkContext arg1) throws Exception{
		String archivo = resource.getFile().toString();
		Path path=Paths.get(archivo);
		Files.delete(path);
		
		return RepeatStatus.FINISHED;
	
	}

}
