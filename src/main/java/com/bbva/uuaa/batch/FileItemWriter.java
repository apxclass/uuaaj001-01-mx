package com.bbva.uuaa.batch;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import com.bbva.uuaa.dto.banco.CuentaDTO;
import com.bbva.uuaa.lib.rj01.UUAARJ01;

public class FileItemWriter implements ItemWriter<CuentaDTO> {

	
	private static final Logger LOGGER = LoggerFactory.getLogger(FileItemWriter.class);
	
	UUAARJ01 uuaaRJ01;
	
	public UUAARJ01 getUuaaRJ01() {
		return uuaaRJ01;
	
	}
	
	public void setUuaaRJ01(UUAARJ01 uuaaRJ01) {
		this.uuaaRJ01=uuaaRJ01;
	}
	
	
	@Override 
	public void write(List<? extends CuentaDTO> arg0) throws Exception{
		
		LOGGER.info("---------------");
		LOGGER.info("cuentas cuentas llegaron {}", arg0.size());
		for (CuentaDTO cuentaDTO : arg0) {
			LOGGER.info("Cuenta{}:",cuentaDTO);
		}
		LOGGER.info("---------------");
		
	}
}
