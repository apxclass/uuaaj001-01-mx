package com.bbva.uuaa.batch;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.bbva.uuaa.dto.banco.CuentaDTO;

public class FileItemProcessor implements ItemProcessor<CuentaDTO,CuentaDTO> {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileItemProcessor.class);

public CuentaDTO process (CuentaDTO arg0)throws Exception{
	LOGGER.info("pasamos por el proceso {}", arg0);
	return arg0;
}
}
