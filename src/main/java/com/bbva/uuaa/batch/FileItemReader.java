package com.bbva.uuaa.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;

import com.bbva.uuaa.dto.banco.CuentaDTO;

public class FileItemReader implements FieldSetMapper<CuentaDTO> {
	private static final Logger LOGGER = LoggerFactory.getLogger(FileItemReader.class);
	
	@Override
	public CuentaDTO mapFieldSet(FieldSet record) throws BindException{
		//CDDIVISA,NUCUENTA,CDTIPOCUENTA,IMPORTE
		
		CuentaDTO cuentaDTO = new CuentaDTO();
		cuentaDTO.setNumCuenta(Long.parseLong(record.readString("CDDIVISA")));
		cuentaDTO.setCdTipoCuenta(record.readString("CDDIVISA"));
		cuentaDTO.setCdDivisa(record.readString("CDDIVISA"));
		cuentaDTO.setImporte (Long.parseLong(record.readString("CDDIVISA")));
		
		
		return cuentaDTO;
	}

}
